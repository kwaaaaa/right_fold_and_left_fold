; right fold
(define (reduce-r list accumulator pattern)
	  (if (null? list)
	    accumulator
		  (reduce-r (cdr list) (pattern (car list) accumulator) pattern)))
  
(display (reduce-r (list 1 2 3 4) 1 -))


(newline)


; left fold
(define (reduce-l list accumulator pattern)
  (define (iter list accumulator)
	  (if (null? list)
	    accumulator
		  (iter (cdr list) (pattern accumulator (car list)))))
		(iter list accumulator pattern))
  
(display (reduce-l (list 1 2 3 4) 1 -))
